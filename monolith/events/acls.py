from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    parameters = {"query": f"{city} {state}"}
    response = requests.get(url, params=parameters, headers=headers)
    content = json.loads(response.content)
    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    pass
